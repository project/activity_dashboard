Activity Dashboard Module
-------------------------------------------
Part of the Drupal and NGOs Project (DaNGO)
-------------------------------------------
Author
Ben Alexander
ben@homemadedigital.com
-------------------------------------------

1. About this module

This module generates a site "dashboard" type feature that can enable site admins to see some top level statistics for their Drupal site illustrated with visually appealing charts, created with a Silverlight library. It is an API only module and is designed to be extended by other developers using a couple of simple hooks. In this very early version, it has the following features:

* A configuration page for setting chart options and styles
* A dashboard overview page to display different types of data
* Individual dashboard pages rendering any number of charts, either as bar or pie charts
* A developer API to allow modules to create dashboard pages
* An example module to demonstrate the API

In future, I would like to add for example:

* Support for other chart types
* Support for other charting libraries
* Different layouts for dashboard pages (2 or 3 column)
* Out of the box support for visualising basic Drupal data, eg. user signups, nodes added, comments added etc.

2. Installation

As per usual, copy to sites/all/modules (or another relevant location). Navigate to the Modules screen and enable. To see the example, also enable activity_dashboard_example.module.

In addition, you will need to download the VisiFire charting library from:

http://www.visifire.com/download_silverlight_charts.php

Extract the files to a location on your hard drive and then find the file SL.Visifire.Charts.xap and Visifire.js in the folder "Silverlight Binaries". Copy these into the module directory within your Drupal installation.

3. API

The module defines two hooks that other developers can implement to get their data included as a dashboard and rendered using some pretty charting:

* hook_activity_dashboard_info - provide metadata such as the name of the dashboard page, description, an icon, and a list of the different datasets that will be displayed
* hook_activity_dashboard_data - provide the actual datasets for the dashboard module to render

See the example module for implementation details.