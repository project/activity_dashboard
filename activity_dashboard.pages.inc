<?php

// $Id:

/**
 * @file
 * Activity dashboard menu callbacks
 */

/**
 * Menu callback - view the dashboard landing page
 */
function activity_dashboard_view_dashboard(){
  
  drupal_add_css(drupal_get_path('module', 'activity_dashboard') . '/activity_dashboard.css','module');
  
  $content = "<div id='activity-dashboard-overview'><p>" . t('Welcome to the dashboard. Below are a list of dashboard areas to view.') . "</p>";
  $content .= "<ul id='activity-dashboard-overview-list'>";
  $implementers = module_implements('activity_dashboard_info');
  $pages = array();
  foreach($implementers as $module){
  	$dashboard_info = module_invoke($module, 'activity_dashboard_info');
  	$pages[$dashboard_info['key']] = array(
  	  'module' => $module,
  	  'name'   => $dashboard_info['name'],
  	  'datasets'  => $dashboard_info['datasets'],
  	);
  	$content .= theme('activity_dashboard_item',$dashboard_info);
  }
  $content .= "</ul>";
  variable_set('activity_dashboard_pages',$pages);
  return $content;
}

/**
 * Menu callback - view an individual dashboard page
 */
function activity_dashboard_view_dashboard_page($pageid){
  
  // check that there is an implementing module for this key
  $pages = activity_dashboard_get_pages($pageid);
  if( !$pages ){
  	return 'No dashboard page found';
  }
  
  // set the title
  drupal_set_title($pages[$pageid]['name']);
  
  // add visifire js lib
  drupal_add_js(drupal_get_path('module', 'activity_dashboard') . '/Visifire.js','module','header');
  
  // create the js to load xml configuration files
  // TODO: is there a way of doing this with a single ajax request? This is horribly inefficient.
  $generated_js = "";
  foreach( $pages[$pageid]['datasets'] as $dataset => $info ){
  	$generated_js .= "$.get('/activity_dashboard_xaml/" . $pageid . "/" . $dataset . "','',function(data, textStatus, XMLHttpRequest){
  		var vChart_" . $dataset . " = new Visifire('/" . drupal_get_path('module', 'activity_dashboard') . "/SL.Visifire.Charts.xap' , " . variable_get('activity_dashboard_width', 500) . " , " . variable_get('activity_dashboard_height', 300) . " );
        vChart_" . $dataset . ".setDataXml(data);
        vChart_" . $dataset . ".render('chart_" . $dataset . "');
  	});
  	";
  }
  drupal_add_js("$(document).ready(function(){" . $generated_js . "});", "inline", "header");
  
  // create the markup
  $content = "<p><a href='/activity_dashboard'><< Back to the dashboard</a></p>";
  foreach( $pages[$pageid]['datasets'] as $dataset => $info ){
    $vars = array();
    $vars['chart_id'] = "chart_" . $dataset;
    $vars['info'] = $info;
  	$content .= theme('activity_dashboard_dataset',$vars);
  }
  
  return $content;
}

/**
 * Menu callback - get xaml config as xml
 */
function activity_dashboard_xaml($pageid,$dataset){
	
  // check that there is an implementing module for this key
  $pages = activity_dashboard_get_pages($pageid);
  if( !$pages ){
  	return 'No dashboard page found';
  }
  
  $datasets = activity_dashboard_get_datasets($pageid);
  $current_dataset = $datasets['datasets'][$dataset];
	
  // load the template xaml
  $xaml = new DOMDocument();
  $xaml->load(dirname(__FILE__) . '/activity_dashboard_xaml.xml');

  // add settings
  $xaml->getElementsByTagName("Chart")->item(0)->setAttribute("ColorSet", variable_get('activity_dashboard_colourset', 'VisiBlue'));
  $xaml->getElementsByTagName("Chart")->item(0)->setAttribute("View3D", variable_get('activity_dashboard_view3D', 'True'));
  $xaml->getElementsByTagName("Chart")->item(0)->setAttribute("BorderThickness", variable_get('activity_dashboard_borderthickness', 0));
  $xaml->getElementsByTagName("Chart")->item(0)->setAttribute("Width", variable_get('activity_dashboard_width', 500));
  $xaml->getElementsByTagName("Chart")->item(0)->setAttribute("Height", variable_get('activity_dashboard_height', 300));

  // add legend
  $xaml->getElementsByTagName("Title")->item(0)->setAttribute("Text",$current_dataset["title"]);
  $xaml->getElementsByTagName("Chart.AxesX")->item(0)->getElementsByTagName("Axis")->item(0)->setAttribute("Title",$current_dataset["x_axis_label"]);
  $xaml->getElementsByTagName("Chart.AxesY")->item(0)->getElementsByTagName("Axis")->item(0)->setAttribute("Title",$current_dataset["y_axis_label"]);
  
  // TODO: add support for multiple data series
  // set chart type
  $xaml->getElementsByTagName("DataSeries")->item(0)->setAttribute("RenderAs", $current_dataset["type"]);
  
  // add data
  foreach($current_dataset["datapoints"] as $label => $value ){
  	$datapoint = $xaml->createElement("vc:DataPoint");
  	$datapoint->setAttribute("AxisXLabel",$label);
  	$datapoint->setAttribute("YValue",$value);
  	$xaml->getElementsByTagName("DataSeries.DataPoints")->item(0)->appendChild($datapoint);
  }

  // output - we have to use saveHTML as Visifire doesn't like the xml declaration
  echo $xaml->saveHTML();
}


