<?php

// $Id:

/**
 * @file
 * Activity dashboard admin pages
 */

/**
 * Main settings form
 */
function activity_dashboard_admin(){
  $form = array();

  $form['activity_dashboard_colourset'] = array(
    '#type'          => 'select',
    '#options'       => array(
                          'Visifire1'   => 'Visifire1',
                          'Visifire2'   => 'Visifire2',
                          'VisiRed'     => 'VisiRed',
                          'VisiBlue'    => 'VisiBlue',
                          'VisiGreen'   => 'VisiGreen',
                          'VisiViolet'  => 'VisiViolet',
                          'VisiGray'    => 'VisiGray',
                          'VisiAqua'    => 'VisiAqua',
                          'VisiOrange'  => 'VisiOrange',
                          'DarkShades'  => 'DarkShades',
                          'Caravan'     => 'Caravan',
                          'Picasso'     => 'Picasso',
                          'DullShades'  => 'DullShades',
                          'SandyShades' => 'SandyShades',
                          'CandleLight' => 'CandleLight',
                        ),
    '#title'         => t('Colour Set'),
    '#default_value' => variable_get('activity_dashboard_colourset', 'VisiBlue'),
    '#description'   => t('Choose the built in colour set for dashboard charts'),
  );
  
  $form['activity_dashboard_view3D'] = array(
    '#type'          => 'select',
    '#options'       => array(
                          'True'   => 'On',
                          'False'   => 'Off',
                        ),
    '#title'         => t('3D Effect'),
    '#default_value' => variable_get('activity_dashboard_view3D', 'True'),
    '#description'   => t('Enable or disable the 3D effect on dashboard charts'),
  );
  
  $form['activity_dashboard_borderthickness'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Border Thickness'),
    '#default_value' => variable_get('activity_dashboard_borderthickness', 0),
    '#description'   => t('Set the thickness of the dashboard charts border - 0 for no border'),
    '#size'          => 2,
    '#maxlength'     => 2,
  );
  
   $form['activity_dashboard_width'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Width'),
    '#default_value' => variable_get('activity_dashboard_width', 500),
    '#description'   => t('Set the width of dashboard charts'),
    '#size'          => 5,
    '#maxlength'     => 5,
  );
  
  $form['activity_dashboard_height'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Height'),
    '#default_value' => variable_get('activity_dashboard_height', 300),
    '#description'   => t('Set the height of dashboard charts'),
    '#size'          => 5,
    '#maxlength'     => 5,
  );

  return system_settings_form($form);
}